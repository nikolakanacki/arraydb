const os = require('os');
const ifaces = os.networkInterfaces();
const crypto = require('crypto');
const uidbase = Math.random().toString() + Object
.keys(ifaces)
.map(key => ifaces[key])
.reduce((col,iface) => { col = col.concat(iface); return col })
.find(iface => !iface.internal && iface.address)
.address + process.pid;
let uidcount = 0;

module.exports.generateUUID = function () {
  return crypto
  .createHash('md5')
  .update(uidbase + (uidcount++),'utf8')
  .digest('hex');
}

module.exports.sortAsc = function (key,array) {
  array.sort((a,b) => {
    return a[key] < b[key]
    ? -1 : a[key] > b[key]
    ?  1 : 0;
  });
}

module.exports.sortDesc = function (key,array) {
  array.sort((a,b) => {
    return a[key] > b[key]
    ? -1 : a[key] < b[key]
    ?  1 : 0;
  });
}

module.exports.sortGeo = function (key,array) {
  const coords = key.split(':');
  coords[2] = coords[2] || 'near';
  const lat1 = parseFloat(coords[1]);
  const lng1 = parseFloat(coords[0]);
  for (let i=0; i<array.length; i++) {
    array[i].__d = module.exports.getDistance(
      lat1,array[i].latitude,
      lng1,array[i].longitude
    );
  }
  if (coords[2] === 'near') {
    return module.exports.sortAsc('__d',array);
  } else {
    return module.exports.sortDesc('__d',array);
  }
}

module.exports.getDistance = function (lat1,lat2,lng1,lng2) {
  const p = 0.017453292519943295;
  const c = Math.cos;
  const a = 0.5-c((lat2-lat1)*p)/2+c(lat1*p)*c(lat2*p)*(1-c((lng2-lng1)*p))/2;
  return Math.asin(Math.sqrt(a));
}
