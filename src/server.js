#!/usr/bin/node

const argv = Object.assign({
  bind: 8008,
},require('minimist')(process.argv.slice(2)));

const net = require('net');
const util = require('./util');
const sift = require('sift');
const debug = require('debug')('arraydb:server');

const TYPE = require('./types');
const ERROR = require('./errors');

const isArray  = require('lodash/isArray');
const isNumber = require('lodash/isFinite');
const isString = require('lodash/isString');
const isObject = require('lodash/isObject');
const isFunction = require('lodash/isFunction');
const isUndefined = require('lodash/isUndefined');

class ArrayDBServer {

  constructor () {
    this._db = {};
    this._server = null;
    this._sockets = {};
    this._handlers = {};
  }

  ERES () {
    return { error: ERROR.GENERIC };
  }

  start (port,host,onStarted) {
    if (this._server) {
      this.stop(() => this.start(port,host,onStarted));
    } else {
      this._sockets = {};
      this._server = new net.Server();
      this._server.on('error',(error) => {
        this.stop(() => setTimeout(() => this.start(port,host,onStarted),1000));
        debug ('server error, restarting',error);
      });
      this._server.on('connection',socket => {
        socket.uuid = util.generateUUID();
        socket.rawbuffer = '';
        socket.on('data',(raw) => {
          const tokens = (socket.rawbuffer + raw).split('\0');
          socket.rawbuffer = tokens.pop();
          tokens.forEach(token => this.receive(token,socket.uuid));
        });
        socket.on('error',error => {
          debug('socket error',error);
          this.destroySocket(socket.uuid);
        });
        socket.on('close',hadError => {
          debug('socket closed, error: %s',hadError);
          this.destroySocket(socket.uuid);
        });
        this._sockets[socket.uuid] = socket;
        debug('connected: %s, have %d client(s)',socket.uuid,Object.keys(this._sockets).length);
      });
      this._server.listen(port,host,onStarted);
    }
    return this;
  }

  stop (cb) {
    if (!this._server) {
      cb && cb ();
    } else {
      this._server.close(() => cb && cb());
      Object.keys(this._sockets).forEach(sid => this.destroySocket(sid));
    }
    delete this._server;
    return this;
  }

  receive (token,sid) {
    try {
      var message = typeof token === 'string' ? JSON.parse(token) : token;
      var mid = message.mid;
      var type = message.type;
      var subject = message.subject;
      var data = message.data;
    } catch (error) {
      return debug('error parsing incoming message: %s',token,error);
      return false;
    }
    switch (type) {
      case TYPE.EXEC:
        return this.exec(data,sid,mid);
      break;
      case TYPE.REQUEST:
        return false
        || this.call(this._handlers[subject],subject,data,sid,mid)
        || this.respond(this.ERES(),sid,mid,subject);
      break;
      case TYPE.BROADCAST:
        return this.broadcast(subject,data,sid,mid);
      break;
      default:
        debug('got undefined type: %s',type,message);
      break;
    }
  }

  call (callback,...args) {
    if (callback) {
      try {
        callback(...args);
        return true;
      } catch (error) {
        debug ('error while running callback',error);
        return false;
      }
    }
    return false;
  }

  destroySocket (sid) {
    try {
      this._sockets[sid].removeAllListeners();
      this._sockets[sid].on('error',() => {});
      this._sockets[sid].destroy();
    } catch (error) {
      debug('error destroying socket',error);
    }
    delete this._sockets[sid];
  }

  send (sid,mid,type,subject,data) {
    if (!sid || !this._sockets[sid]) {
      debug('trying to send message to a socket that does not exist: %s',sid);
      return false;
    }
    try {
      var message = JSON.stringify({
        mid: mid,
        type: type,
        subject: subject,
        data: data || {},
      });
      this._sockets[sid].write(message + '\0');
      return true;
    } catch (error) {
      debug('error while sending message',error);
      return false
    }
  }

  respond (data,sid,mid,subject=null) {
    if (!mid) {
      debug('no point in responding without message id');
      return false;
    } else {
      return this.send(sid,mid,TYPE.RESPONSE,subject||util.generateUUID(),data);
    }
  }

  broadcast (subject,data,sid,mid) {
    Object
    .keys(this._sockets)
    .forEach(uuid => uuid !== sid && this
    .send(uuid,mid,TYPE.BROADCAST,subject,data));
    return true;
  }

  exec (data,sid,mid) {
    if (!data) {
      debug ('exec has no data to operate on, got:',data);
      return this.respond(this.ERES(),sid,mid);
    } else {
      try {
        // Setup defaults
        if (isUndefined(data.treatNullAsUndefined)) {
          data.treatNullAsUndefined = true;
        }
        if (isUndefined(data.return)) {
          data.return = true;
        }
        if (isUndefined(data.upsert)) {
          data.upsert = true;
        }
        if (isUndefined(data.update)) {
          data.update = true;
        }
        if (!isString(data.model)) {
          data.model = 'Test';
        }
        if (!isObject(data.query)) {
          data.query = {};
        }
        // Params
        const db = this._db;
        const now = Date.now();
        const model = data.model;
        // Execute drop
        if (isArray(data.drop)) {
          db[model] = data.drop;
        }
        // Execute query
        let results = [];
        let sifter = data.sift ? sift(data.query) : false;
        db[model] = (db[model]||[]).filter(doc => {
          if (isObject(doc._expiresAt)) {
            if (isNumber(doc._expiresAt._) && doc._expiresAt._ <= now) {
              // early exit if whole doc should have been expired
              return false;
            } else {
              const expKeys = Object.keys(doc._expiresAt);
              if (!expKeys.length) {
                delete doc._expiresAt;
              } else if (!(expKeys.length === 1 && expKeys[0] === '_')) {
                expKeys.forEach(key => {
                  if (!isNumber(doc._expiresAt[key])) {
                    delete doc._expiresAt[key];
                  } else if (doc._expiresAt[key] <= now) {
                    delete doc._expiresAt[key];
                    delete doc[key];
                  }
                });
              }
            }
          }
          if (sifter) {
            if (sifter(doc)) {
              results.push(doc);
            }
          } else if (
            !Object.keys(data.query).find(key =>
              data.treatNullAsUndefined === true && data.query[key] === null
              ? typeof doc[key] !== 'undefined'
              : data.query[key] !== doc[key])
          ) {
            results.push(doc);
          }
          return true;
        });
        // Execute sort
        if (isString(data.sort) && data.sort !== 'NONE') {
          const sort = data.sort.split(' ');
          sort[1] = sort[1] || 'ASC';
          switch (sort[1]) {
            case 'ASC':
              util.sortAsc(sort[0],results);
            break;
            case 'DESC':
              util.sortDesc(sort[0],results);
            break;
            case 'GEO':
              util.sortGeo(sort[0],results);
            break;
          }
        }
        // Execute limit
        if (isNumber(data.limit) && data.limit > 0 && results.length > data.limit) {
          results = results.slice(0,data.limit);
        }
        // Execute write
        if (isObject(data.write)) {
          if (!results.length) {
            if (data.upsert) {
              let doc = Object.assign({},data.write);
              db[model].unshift(doc);
              results.unshift(doc);
            }
          } else if (data.update) {
            let expiresAt = isObject(data.write._expiresAt)
            ? data.write._expiresAt
            : false;
            delete data.write._expiresAt;
            results.forEach(doc => {
              Object.assign(doc,data.write);
              if (expiresAt) {
                doc._expiresAt = Object.assign(doc._expiresAt||{},expiresAt);
              }
            });
          }
        }
        // Execute remove
        if (data.remove) {
          db[model] = db[model].filter(doc => results.indexOf(doc) === -1);
        }
        // Send results if sid and mid
        if (sid && mid) {
          return this.respond(data.return ? results : [],sid,mid);
        } else {
          return true;
        }
      } catch (error) {
        debug ('error on exec',error);
        return this.respond(this.ERES(),sid,mid);
      }
    }
  }

  registerRequestHandler (subject,callback) {
    this._handlers[subject] = callback;
  }

}

ArrayDBServer.ERROR = ERROR;
ArrayDBServer.TYPE  = TYPE;

module.exports = ArrayDBServer;
