const net = require('net');
const util = require('./util');
const debug = require('debug')('arraydb:client');
const ecb = (cb) => typeof cb === 'function' && cb();

const TYPE = require('./types');
const ERROR = require('./errors');

class ArrayDBClient {

  constructor () {
    this._id = util.generateUUID();
    this._counter = 0;
    this._intents = {};
    this._subscriptions = {};
  }

  ERES () {
    return { error: ERROR.GENERIC };
  }

  connect (port,host,onConnect,onReconnect) {
    if (this._requests) {
      Object
      .keys(this._requests)
      .forEach(key => this
      .call(this._requests[key],this.ERES()))
    }
    this._requests = {};
    this._connected = false;
    this._connectedTimes = 0;
    this._buffer = '';
    if (this._socket) {
      try {
        this._socket.removeAllListeners();
        this._socket.on('error',() => {});
        this._socket.destroy();
        delete this._socket;
      } catch (error) {
        debug('error destroying socket',error);
      }
    }
    const socket = this._socket = new net.Socket();
    socket.setEncoding('utf8');
    socket.setNoDelay(true);
    socket.setMaxListeners(Infinity);
    socket.connect(port,host,() => {
      this._connected = true;
      debug('connected: %d',this._connectedTimes);
      this._connectedTimes > 0
      ? (onReconnect && onReconnect())
      : (onConnect   && onConnect());
      this._connectedTimes++;
    });
    socket.on('error',(error) => {
      this._connected = false;
      debug('socket error',error);
      setTimeout(() => this.connect(port,host,onConnect,onReconnect),1000);
    });
    socket.on('close',() => {
      this._connected = false;
      debug('socket closed unexpectedly');
      setTimeout(() => this.connect(port,host,onConnect,onReconnect),1000);
    });
    socket.on('data',(raw) => {
      const tokens = (this._buffer + raw).split('\0');
      this._buffer = tokens.pop();
      tokens.forEach(token => this.receive(token));
    });
    return this;
  }

  isConnected () {
    return !!this._connected;
  }

  receive (token) {
    try {
      var message = typeof token === 'string' ? JSON.parse(token) : token;
      var mid = message.mid;
      var type = message.type;
      var subject = message.subject;
      var data = message.data || {};
    } catch (error) {
      debug('error parsing incoming message: %s',token,error);
      return false;
    }
    switch (type) {
      case TYPE.RESPONSE:
        return this.call(this._requests[mid],data);
      break;
      case TYPE.BROADCAST:
        debug('%s got broadcast id: %s',this._id,mid,subject,this._subscriptions[subject]);
        if (this._subscriptions[subject]) {
          Object
          .keys(this._subscriptions[subject])
          .forEach(key => debug(key) || this
          .call(this._subscriptions[subject][key],data));
          return true;
        } else {
          return false;
        }
      break;
      default:
        debug('got undefined type: %s',type,message);
        return false;
      break;
    }
    return false;
  }

  send (mid,type,subject,data) {
    if (!this._connected) {
      debug('cannot send, not connected',mid,type,subject,data);
      return false;
    }
    try {
      var message = JSON.stringify({
        mid: mid,
        type: type,
        subject: subject,
        data: data,
      });
      this._socket.write(message + '\0');
      return true;
    } catch (error) {
      debug('error while sending message',error);
      return false;
    }
  }

  call (callback,...args) {
    if (callback) {
      try {
        callback(...args);
        return true;
      } catch (error) {
        debug ('error while running callback',error);
        return false;
      }
    } else {
      debug ('invoking function that does not exist',callback)
    }
    return false;
  }

  subscribe (subject,callback) {
    const id = (++this._counter).toString();
    this._subscriptions[subject] = this._subscriptions[subject] || {};
    this._subscriptions[subject][id] = callback;
    return () => this._subscriptions[subject] && delete this._subscriptions[subject][id];
  }

  broadcast (subject,data) {
    const mid = util.generateUUID();
    debug('%s sending broadcast id: %s',this._id,mid);
    return this.send(mid,TYPE.BROADCAST,subject,data);
  }

  request (subject,data,callback,isExec) {
    const mid = util.generateUUID();
    const sended = this.send(mid,isExec
    ? TYPE.EXEC
    : TYPE.REQUEST,subject,data);
    if (sended) {
      this._requests[mid] = callback;
      return true;
    } else {
      this.call(callback,this.ERES());
      return false;
    }
  }

  exec (config,cb) {
    cb = typeof cb === 'function' ? cb : ecb;
    if (typeof config.model !== 'string' || !config.model.length) {
      setTimeout(() => cb(this.ERES()));
    }
    return this.request(null,config,cb,true);
  }

  get (model,id,cb) {
    this.exec({
      model: model,
      query: { id:id },
    },cb);
    return this;
  }

  set (model,id,write,cb) {
    this.exec({
      model: model,
      query: { id:id },
      write: Object.assign({},write,{ id:id }),
    },cb);
    return this;
  }

  lock (config,cb) {
    config = Object.assign({
      intent: 'lock',
      query: {},
      lasts: 15000,
      sift: false,
    },config||{});
    const tester = new RegExp('^'+config.intent+'@');
    const iterator =
       this._intents[config.intent] =
    (((this._intents[config.intent]||0)+1)%1000000);
    const lock = `${config.intent}@${util.generateUUID()}`;
    const unlock = (ucb) => this.exec({
      model: config.model,
      query: { [lock]: true },
      upsert: false,
      write: { _expiresAt: { [lock]: 0 } }
    },ucb);
    this.exec({
      sift:  config.sift,
      model: config.model,
      query: config.query,
      upsert: false,
      write: { [lock]: true, _expiresAt: { [lock]: Date.now() + config.lasts } }
    },res => {
      if (res.error) {
        unlock (() => cb (res,ecb));
      } else {
        const granted = !res.find(doc => !!Object.keys(doc).find(key => key !== lock && tester.test(key)));
        if (!granted) {
          unlock (() => cb (this.ERES(),ecb));
        } else {
          cb (res,unlock);
        }
      }
    });
  }

}

ArrayDBClient.ERROR = ERROR;
ArrayDBClient.TYPE  = TYPE;

module.exports = ArrayDBClient;
