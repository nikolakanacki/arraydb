module.exports.REQUEST = 'TYPE_REQUEST';
module.exports.RESPONSE = 'TYPE_RESPONSE';
module.exports.BROADCAST = 'TYPE_BROADCAST';
module.exports.EXEC = 'TYPE_EXEC';
