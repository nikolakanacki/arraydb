const ArrayDBClient = require('./src/client');
const ArrayDBServer = require('./src/server');
const cp = require('child_process');
const host = '127.0.0.1'
const port = 8009;
const cwd = process.cwd();

class ArrayDBServerWithGate extends ArrayDBServer {
  constructor () {
    super ();
    this.didTestGate = false;
  }
  exec (data,sid,mid) {
    if (!this.didTestGate) {
      this.didTestGate = true;
      super.respond({ gate:true },sid,mid);
    } else {
      super.exec(data,sid,mid);
    }
  }
}

const s1 = new ArrayDBServerWithGate();
const c1 = new ArrayDBClient();

test('error before connected',done => {
  c1.exec({
    model: 'TestModel',
    query: {}
  },res => {
    expect(res.error).not.toBe(undefined);
    done();
  });
});

test('starting server',done => {
  s1.start(port,host,() => {
    done();
  });
});

test('connecting',done => {
  c1.connect(port,host,() => {
    done();
  });
});

test('server gate',done => {
  c1.exec({
    model: 'TestModel',
    query: { 'testing':'gate' },
  },res => {
    expect(res.gate).toBe(true);
    done();
  });
});

test('serverside exec',done => {
  s1.exec({
    model: 'ServerSide',
    drop:  [{ id:'test' }]
  });
  c1.exec({
    model: 'ServerSide',
    query: {}
  },res => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test');
    done();
  });
});

test('insert document',done => {
  c1.set('TestModel','test',{
    value: 10,
  },(res) => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test');
    expect(res[0].value).toBe(10);
    done();
  });
});

test('update document',done => {
  c1.exec({
    model: 'TestModel',
    query: { id: 'test' },
    write: { value: 9 },
  },(res) => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test');
    expect(res[0].value).toBe(9);
    done();
  });
});

test('add document through query (that does not match any results) + write',done => {
  c1.exec({
    model: 'TestModel',
    query: { id: 'test-1' },
    write: { id: 'test-1',value: 8 },
  },(res) => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test-1');
    expect(res[0].value).toBe(8);
    done();
  });
});

test('do not add document through query that matches zero results if upsert is false',done => {
  c1.exec({
    model: 'TestModel',
    query: { id: 'notInDatabase' },
    write: { id: 'notInDatabase', value: 10 },
    upsert: false,
  },res => {
    expect(res.length).toBe(0);
    c1.exec({
      model: 'TestModel',
      query: { id: 'notInDatabase' }
    },res => {
      expect(res.length).toBe(0);
      done();
    });
  });
});

test('get all documents and sort them',done => {
  c1.exec({
    model: 'TestModel',
    sort: 'value DESC',
  },(res) => {
    expect(res.length).toBe(2);
    expect(res[0].value).toBe(9);
    expect(res[1].value).toBe(8);
    done();
  });
});

test('get documents which match the query',done => {
  c1.exec({
    model: 'TestModel',
    query: { id:'test-1' }
  },(res) => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test-1');
    done();
  });
});

test('sort documents by geolocation (near/far)',done => {
  c1.exec({
    model: 'TestGeoModel',
    query: { id: 'far' },
    write: {
      id: 'far',
      latitude: 19.926232,
      longitude: 10.129395,
    }
  },(res1) => {
    expect(res1[0].id).toBe('far');
    c1.exec({
      model: 'TestGeoModel',
      query: { id: 'near' },
      write: {
        id: 'near',
        latitude: 19.512551,
        longitude: 5.471191,
      }
    },(res2) => {
      expect(res2[0].id).toBe('near');
      c1.exec({
        model: 'TestGeoModel',
        sort:  '-5.537109:19.491839 GEO', // "<lng>:<lat>:<near|far?> GEO"
      },(res) => {
        expect(res.length).toBe(2);
        expect(res[0].id).toBe('near');
        done();
      });
    });
  })
});

test('limit returned results',done => {
  c1.exec({
    model: 'TestGeoModel',
    limit: 1
  },(res) => {
    expect(res.length).toBe(1);
    done();
  });
});

test('removing documents',done => {
  c1.exec({
    model: 'TestGeoModel',
    limit: 1,
    remove: true,
  },(res) => {
    expect(res.length).toBe(1);
    c1.exec({
      model: 'TestGeoModel'
    },(res) => {
      expect(res.length).toBe(1);
      done();
    });
  });
});

test('expiring documents',done => {
  then = Date.now() + 100;
  c1.exec({
    model: 'TestModel',
    query: { 'id':'test-2' },
    write: { 'id':'test-2', _expiresAt:{ _:then } },
  },(res) => {
    expect(res.length).toBe(1);
    expect(res[0].id).toBe('test-2');
    expect(res[0]._expiresAt._).toBe(then);
    setTimeout(() => {
      c1.exec({
        model: 'TestModel',
        query: { 'id':'test-2' },
      },(res) => {
        expect(res.length).toBe(0);
        done();
      });
    },500);
  });
});

test('expiring document keys',done => {
  c1.exec({
    model: 'TestModel',
    drop: [
      { id: 'test-1', value: 'test' },
      { id: 'test-2', value: 'test' },
      { id: 'test-3', value: 'test' },
    ]
  },res => {
    expect(res.length).toBe(3);
    const then = Date.now() + 100;
    c1.exec({
      model: 'TestModel',
      query: { id:'test-2' },
      write: { _expiresAt: { value: then } }
    },res => {
      expect(res[0]._expiresAt.value).toBe(then);
      setTimeout(() => {
        c1.exec({
          model: 'TestModel',
          query: { id:'test-2' },
        },res => {
          expect(res[0].value).toBe(undefined);
          done();
        })
      },500)
    })
  });
})

test('droping model with new data',done => {
  c1.exec({
    model: 'TestModel',
    drop:  [{
      id: 'test-1'
    },{
      id: 'test-2'
    },{
      id: 'test-3'
    }]
  },res => {
    expect(res.length).toBe(3);
    expect(res[0].id).toBe('test-1');
    expect(res[2].id).toBe('test-3');
    done();
  });
});

test('return zero results',done => {
  c1.exec({
    model: 'TestModel',
    return: false,
  },res => {
    expect(res.length).toBe(0);
    done();
  });
});

test('sift query',done => {
  c1.exec({
    model: 'TestModel',
    drop: new Array(200).join(' . ').split('.').map((a,i) => ({ id:i })),
  },res => {
    expect(res.length).toBe(200);
    c1.exec({
      model: 'TestModel',
      query: {
        id: {
          $nin: [1,2,3,4,5]
        }
      },
      sift: true
    },res => {
      expect(res.length).toBe(195);
      expect(res[0].id).toBe(0);
      expect(res[1].id).toBe(6);
      done();
    });
  });
});

test('treat null as undefined',done => {
  c1.exec({
    model: 'TestModel',
    drop: new Array(200).join(' . ').split('.').map((a,i) => {
      const doc = { id: i };
      if (i%2) {
        doc.value = 1;
      }
      return doc;
    })
  },res => {
    expect(res.length).toBe(200);
    c1.exec({
      model: 'TestModel',
      query: { value: null }
    },res => {
      expect(res.length).toBe(100);
      expect(res[0].value).toBe(undefined);
      c1.exec({
        model: 'TestModel',
        query: { value: null },
        treatNullAsUndefined: false
      },res => {
        expect(res.length).toBe(0);
        done();
      });
    });
  });
});

test('intent locking system',done => {
  c1.exec({
    model: 'TestModel',
    drop: new Array(200).join(' . ').split('.').map((a,i) => ({ id: i }))
  },res => {
    expect(res.length).toBe(200);
    c1.lock({
      model: 'TestModel',
      query: { id: { $in: [1,2,3,4] } },
      sift: true,
    },(res,unlock) => {
      expect(res.length).toBe(4);
      c1.lock({
        model: 'TestModel',
        query: { id: { $in: [3,4,5,6] } },
        sift: true,
      },(errorRes) => {
        expect(errorRes.error).toBe(ArrayDBClient.ERROR.GENERIC);
        unlock(() => {
          c1.exec({
            model: 'TestModel',
            query: {},
          },cleanRes => {
            expect(cleanRes.length).toBe(200);
            expect(cleanRes.find(doc => Object.keys(doc).find(key => /^lock@/.test(key)))).toBe(undefined);
            done();
          });
        });
      });
    });
  });
});

test('dont update but upsert',done => {
  c1.exec({
    model: 'TestModel',
    drop: new Array(200).join(' . ').split('.').map((a,i) => ({ id: i }))
  },res => {
    expect(res.length).toBe(200);
    c1.exec({
      model: 'TestModel',
      update: false,
      upsert: false,
      query: {},
      write: {
        id: 2000
      },
    },res => {
      expect(res.length).toBe(200);
      expect(res.find(doc => doc.id === 2000)).toBe(undefined);
      done();
    });
  });
});

test('broadcast messages',done => {
  const onDone = () => --onDone.counter && done(); onDone.counter = 2;
  const c2 = new ArrayDBClient().connect(port,host,() => {
    const c3 = new ArrayDBClient().connect(port,host,() => {
      c1.subscribe('test',(data) => {
        expect(data.test).toBe(true);
        onDone();
      });
      c2.subscribe('test',(data) => {
        expect(data.test).toBe(true);
        onDone();
      });
      c3.broadcast('test',{ test: true });
    })
  });
})
