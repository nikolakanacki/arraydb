# ArrayDB
---

Generalno laka za upotrebu, malo drugacija od kako inace baze rade (afaik) tho.

Zasniva se na tome da ne postoje prave CRUD operacije, vec svaka `exec` operacija izvrsava sledece korake (uvek, istim redosledom):

1. **DROP** - tehnicki radi replace celog modela (svih dokumenata) sa passovanim arrayom,
2. **QUERY** - napuni `matches` array sa svime sto matchuje query (default query je prazan - matchuje sve)
3. **SORT** - sortira `matches` array po datom kriterijumu, veoma slicno kako loopback radi, vidi dole
4. **LIMIT** - discarduje iz `matches` array-a sve nakon limita, ostavljajucu u njemu "at most" `<limit>` broj rezultata
5. **WRITE** - ukoliko imamo write key kao objekat, shallow-mergeujemo ga u svaki doc iz `matches` array-a, ukoliko je `matches` prazan, dodamo prazan doc i onda merge-ujemo u njega (defacto `insert` ali samo ukoliko `<object-config>.upsert` nije `false`, pogledaj dole),
6. **REMOVE** - ukoliko imamo remove: true, vadimo iz baze svaki doc koji se nalazi u `matches` array-u

Pored `exec` metode, postoje dve skracenice koje rade `set` i `get` tradicionalno, po `id`-u.

# Usage
---

## Server
### Standalone
```
$ cd <path-to-repo>
$ ./arraydb/server.js --bind <port|address:port>
```
### From node:
```
const ArrayDBServer = require('<path-to-server-file>');
const adbBind1 = <int-port|string-address:int-port>
const adbBind2 = <int-port|string-address:int-port>
const server1 = ArrayDBServer.start(adbBind1);
const server2 = ArrayDBServer.start(adbBind2);
```

## Client
```
const ArrayDBClient = require('<path-to-client>`);
const adbBind = <int-port|string-address:int-port>;
const client = new ArrayDBClient(adbBind);

client.exec(<object-config>,<function-cb>);

client.set(<string-model>,<string-id>,<object-write>,<function-cb>);
client.get(<string-model>,<string-id>,<function-cb>);
```

# API
---

# Server

## Parameters `ArrayDBServer.start(<bind>,<gate?>)`

Pokrece server.

### `<bind> <int-port|string-address:int-port>`

Prvi parametar, definise adresu i port (ili samo port) na koji ce server da slusa. Primer `8008` ili `127.0.0.1:8008`.

### `<gate> <function?>`

Drugi parametar, neobavezan, bice prosledjen kao funkcija koja je prvi middleware pri svakom
dolaznom requestu. Dobija jedan argument, `<object-message>` sledeceg formata:
```
{
  data:  <object>   // data koja dolazi sa requestom
  next:  <function> // pozovi da ides na sledeci middleware
  reply: <function> // pozovi da zavrsis request sa argumentom koji cini odgovor
}
```

### `ArrayDBServer<instance>.exec(<object-config>)`

Metod koji se nalazi na instanci servera, radi isto sto i klijent (pogledaj klijenta za api).
Generalno odlican za inicijalizaciju. Nema callback jer je operacija
sinhrona.

# Client

## `ArrayDBClient<constructor>()`

Konstruktor ne prima argumente, samo napravi mrtvu instancu klijenta.
Da bi se pokrenuo potrebno je pozvati `ArrayDBClient<instance>.connect` metod (pogledaj dole).

## `ArrayDBClient<instance>.connect(<bind>,<cb?>)`

### `<bind> <int-port|string-address:int-port>`

Prvi i jedini parametar, definise adresu i port (ili samo port) gde slusa server sa kojim klijent zeli da se poveze. Primer `8008` ili `127.0.0.1:8008`.

### `<cb> <function?>`

Drugi argument je callback koji ce biti pozvan kada je konekcija uspesna. Pre nego sto se desi ovaj
callback klijent ce vracati error response -1 sto znaci da nije konektovan.

## `ArrayDBClient<instance>.exec(<object-config>,<function-cb>)`

Glavni metod za komunikaciju sa databazom. Pogledaj dole za api i argumente.

### `<object-config>.model <string>`

Definise za koji model se radi operacija, generalno bilo koji string. Obavezan.

### `<object-config>.drop <array>`

Ukoliko je sam `drop` key prisutan kao validan array, zamenice ceo model sa njime. Passuj prazan array da ocistis model.

### `<object-config>.query <object?>`

Definise query sa kojim generisemo array rezultata zvan `matches`.
Sve dalje se izvrsava na tom arrayu (ciji dokumenti su references iz databaze, sto znaci da sve operacije na njima direktno idu u bazu). Format je `<key>: <value>` i podrzava samo strict matching (jednako ili nije jednako). Po defaultu je prazan, sto znaci da matchuje sve dokumente.

Druga vrsta query-a koju podrzavamo jeste ona koju nudi `sift` module, i to je full blown query language veoma slican onom koji koristi MongoDB.
Isti query objekat se koristi, samo treba definisati `<object-consig>.sift = true`.

Za detaljnije mogucnosti `sift`-a, pogledati dokumentaciju: https://github.com/crcn/sift.js

### `<object-config>.treatNullAsUndefined <bool?:true>`

Po defaultu, `null` se tretira kao `undefined` sto znaci da bez `sift` qeury-a
mogu da se traze svi dokumenti iz modela koji nemaju (imaju odsustvo, `undefined`)
value za dat key. Na primer, query oblika `{ value: null }` bi vratilo dokumente
koji uopste nemaju setovan `value` key.

Ovo ponasanje moze da se iskljuci time sto se podesi key `treatNullAsUndefined`
kao explicit `false`.

### `<object-config>.sift <bool?:false>`

Omogucava koriscenje `sift` modula za query, pogledaj `<object-config>.qeury`.

### `<object-config>.sort <string?:NONE>`

Definise sortiranje `matches` array-a. Ukoliko nije prisutan, rezultati su sortirani je kako su se belezili u bazi (obrnutim redosledom od pisanja, zadnji insertovan dokument je prvi). Format je `<key> <ASC|DESC?:ASC>` za normalan sort (default je `ASC`). Za geosort (near/far), format je `<float-lng>:<float-lat>:<string-type(near|far):near?> GEO`. Za enum type je default `near`. Radi tako sto izracuna distance za svaki dokument prilikom cega ih zabelezi u taj dokument pod `__d` keyem. Za to racunanje koristi polja `latitude` i `longitude` koje nadje na dokumentu (i koje svaki dokument za taj model mora da ima da bi sort bio moguc). Nakon toga samo sortira po distanci. Veoma, veoma brz.

### `<object-config>.limit <int?:-1>`

Smanjuje `matches` array na broj koji je definisan.

### `<object-config>.write <object?>`

Ukoliko je prisutan, za svaki dokument koji se trenutno nalazi u `matches` arrayu odradice "shallow merge" (prekucati vrednosti sa datog objekta na dokument). Ukoliko je `matches` array prazan i `upsert` je `true` (default, pogledaj ispod), napravice novi dokument, staviti ga u `matches` array i u databazu za dati model, i onda uraditi gorepomenuti korak.

### `<object-config>.upsert <bool?:true>`

Ukoliko je eksplicitno stavljen kao `false`, necemo praviti novi dokument kada imamo prisutan `write` a `matches` array je prazan.

### `<object-config>.update <bool?:true>`

Ukoliko je eksplicitno stavljen kao `talse`, necemo updateovati nijedan dokument prisutan u `matches` array-u prilikom `write` koraka. Kombinacijom `upsert=true` i `update=false` mozemo postici `findOrInsert`.

### `<object-config>.remove <bool?:false>`

Ukoliko je vrednost `true`, svaki dokument koji se trenutno nalazi u `matches` array-u bice izbrisan iz databaze, ali ne i iz `matches` arraya (cime ce efektivno biti vracen kao rezultat operacije posto je ovo poslednji korak).

### `<object-config>.return <bool?:true>`

Ukoliko je passovan explicit false, uvek vraca prazan array. Ovo je nacin recimo da izbegnes vracanje puno podataka preko socketa kada si samo hteo drop, recimo.

### `<function-cb>`

Callback.

## `ArrayDBClient<instance>.lock(<object-lock-config>,<function-cb>)`

Pozivom ovog metoda trazite `lock` na dokumente. Pogledaj dole za konfiguraciju.

### `<object-lock-config>.model <string>`

Obavezan, isti kao za `<object-config>.model`.

### `<object-lock-config>.query <object?>`

Isti kao za `<object-config>.query`, limitira rezultate za koje ce se postaviti lock.

### `<object-lock-config>.sift <bool?:false>`

Isti kao za `<object-config>.sift`.

### `<object-lock-config>.lasts <int?:15000>`

Milisekunde kolko je ovom lock-u maximalno dozvoljeno da traje nakon cega ce se sam
otkljucati. Default je 15000 (15 sekundi).

## Arguments: `<function-cb>`

### `0 <results-array>`

Prvi i jedini argument predat callback funkciji prilikom exec operacije. Ili je array koji ima 0 ili vise dokumenata (`matches` array nakon svih izvrsenih operacija) ili je objekat sa `error` keyem (sto znaci da je request bio neuspesan).

### `1 <function-unlock>`

U slucaju `ArrayDBClient<instance>.lock` poziva, drugi argument cini funkcija cijim pozivom upucujemo
`unlock` operaciju na vec zakljucane dokumente. Prima jedan argument - opcionu funkciju koja ce biti
prosledjena `ArrayDBClient<instance>.exec` call-u koji otkljucava dokumente.

# Document
---

Apsolutno free-form, doduse neka konvencija je (da bi set/get skracenice radile) da koristimo polje `id` kao `primary-key` (iako nemamo "primary-key" poredak uopste, za sada). Ima sledece keyeve koji imaju specijalno znacenje:

### `__d`

Ovaj key ce se naci na svim dokumentima modela na kojem je radjen geo-sort. Nosice neku koeficijentsku distancu od sort subjekta po cemu i radimo sort, i vrednost ce uvek biti prekucana od zadnjeg sortiranja. Dupli double-dash ima jer nije obavezno prisutan, ali ipak moze naci primenu kod klijenta (recimo da odstranis sve koji su dalji od `n`).

### `_expiresAt`

Key koji moze da se setuje kroz write. Njegov value je objekat ciji keyevi su integer milliseconds od unix epoch-a nakon cijeg isteka (`now >= <value>`) definisani key se brise sa dokumenta. Key `_` se tretira specijalno i oznacava da se ceo dokument brize nakon definisanog timestamp-a. Primer:
```
{
  _: <whole-document-expires-at-after-this-timestamp>,
  someKey: <this-key-only-expires-after-this-timestamp>
}
```

# Tests
---

```
$ cd <path-to-repo>
$ ./node_modules/.bin/jest ./arraydb/test.js
```

# Notes
---

Jedini pravi metod koji salje komande databazi je `client.exec`, `client.get` je wrapper za:
```
client.exec({
  model: <string-model>,
  query: { id:<string-id> },
},<function-cb>);
```
i `client.set` wrapper za:
```
client.exec({
  model: <string-model>,
  query: { id: <string-id> },
  write:  Object.assign({},<object-write>,{ id: <string-id> }),
},<function-cb>);
```

Polja kao sto su `createdAt` i `updatedAt` nisu enforce-ovana server stranom, ali lako mogu da se naprave sa klijent strane time sto bi ih passovao kroz `write`. Ukoliko postoji potreba za server-side implementacijom, nije problem.

Bench sam radio, na oko milion dokumenata u single modelu krene da trokira malo jace, oko 180ms rekao bih, jer nisam radio optimizacije nikakve jos. Inace na svemu realno nasem usecase-u trebao bi da performe-uje dobro, na nekih 200-300 dokumenata (vozaci, jaka sezona :D) sort po geo-u mi je bio nekih ~1ms

# Roadmap
---

- (maybe?) Add primary key paradime
