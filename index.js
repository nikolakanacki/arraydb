const util = require('./src/util');

module.exports.Client = require('./src/client');
module.exports.Server = require('./src/server');
module.exports.genUID = util.generateUUID;
